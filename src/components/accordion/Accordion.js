import React, { PureComponent } from 'react'

import { AccordionSection } from './accordion-section'

import './Accordion.scss'

class Accordion extends PureComponent {
	constructor(props) {
		super(props)

		const openSections = []
		if (props.children){
			props.children.forEach((child, idx) => {
				if (child.props.isOpen) {
					openSections[idx] = true
				}
			})
		}
		this.state = { openSections }

		this.handleAccordionToggle = this.handleAccordionToggle.bind(this)
	}

	handleAccordionToggle = index => () => {
		const { allowMultipleOpen } = this.props

		this.setState(prevState => {
			const openSections = [...prevState.openSections]

			if (!allowMultipleOpen) {
				openSections.fill(false)
			}
			openSections[index] = !prevState.openSections[index]

			return ({ openSections })
		})
	}

	render() {
		const {
			handleAccordionToggle,
			props: { children = [] },
			state: { openSections },
		} = this

		return (
			<div className='accordion'>
				{children.map(({props: {label, children}}, idx) => (
					<AccordionSection
						label={label}
						onClick={handleAccordionToggle(idx)}
						isOpen={openSections[idx]}
						key={idx}
					>
						{children}
					</AccordionSection>
				))}
			</div>
		)
	}
}

export default Accordion
