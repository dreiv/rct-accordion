import React, { memo } from 'react'

import './AccordionSection.scss'

const AccordionSection = memo(({
	onClick,
	isOpen,
	label,
	children
}) => {
	let className

	if (isOpen) {
		className = "open";
	}

	return (
		<div className="accordion-section">
			<header
				onClick={onClick}
				className={className}
			>
				{label}
				<span> {isOpen ? '△' : '▽'}</span>
			</header>

			{isOpen && (
				<section>
					{children}
				</section>
			)}
		</div>
	)
})


export default AccordionSection
